<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notificacion extends Model
{
    //
        /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'notificacion';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable =
    [
        'id','titulo','imagen','contacto','email','telefono','descripcion','link','icono'
    ];
}
