<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificacionDetalle extends Model
{
       //
        /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'notificacionDetalle';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable =
    [
      'id', 'idAfiliado', 'accion','idNotificacion','nombre','empresa'
    ];
}
