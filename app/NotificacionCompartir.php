<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificacionCompartir extends Model
{
    protected $table = 'notificacionCompartir';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable =
    [
      'id', 'idAfiliado', 'correo','nombre','idNotificacion','empresa'
    ];
}
