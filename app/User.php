<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'afiliados';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = ['idPersonal', 'clave'
     , 'name_em', 'contacto', 'rep_legal', 'rep_emp', 'calle', 'numero', 'interior', 'colonia', 'municipio', 'estado', 'direccion', 'cp', 'telefono', 'tel_movil', 'user_email', 'razon_s', 'pag_web', 'sector', 'giro', 'tamano', 'certificados', 'n_trabaj', 'date_added', 'tipo_afiliacion', 'rfc', 'tipo_dir', 'scian_1', 'scian_2', 'scian_3', 'scian_4', 'scian_5', 'facebook', 'twitter', 'instagram', 'otro', 'certificado_1', 'certificado_2', 'certificado_3', 'logo', 'publicidad', 'ps1', 'ps2', 'ps3', 'ps4', 'ps5', 'fecha_termino', 'fecha_inicio', 'internacional', 'estado_afiliacion',
   ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
