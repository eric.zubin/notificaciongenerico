<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notificacion;
use App\NotificacionCompartir;
use App\NotificacionDetalle;
use Storage;
use View;
use Session;
use OneSignal;
use DB;
use Twilio\Rest\Client;
use Twilio\TwiML\MessagingResponse;


class NotificacionController extends Controller
{
    //
    public function whatsandbox(Request $request){

         $response = new MessagingResponse();
        $message = $response->message('');
        $message->body('Hello World!');
        $response->redirect('https://demo.twilio.com/welcome/sms/');

        echo $response;
       }

    public function mandarwha(){
     /*   $whatsmsapi = new WhatsmsApi();
        $whatsmsapi->setApiKey("5d5d787c224f7");
        $whatsmsapi->sendSms("521371437", "Hello World!");*/

        $sid    = "AC88795ed235e37181b3aa969153c8913e";
        $token  = "7f7c1996b953db35652c788e9fda5fdd";
        $twilio = new Client($sid, $token);

        $message = // Use the client to do fun stuff like send text messages!
        /*$twilio->messages->create(
            // the number you'd like to send the message to
            '+524421371437',
            array(
                // A Twilio phone number you purchased at twilio.com/console
                'from' => '+19282556840',
                // the body of the text message you'd like to send
                "body" => "Hi Joe! Thanks for placing an order with us. We’ll let you know once your order has been processed and delivered. Your order number is O12235234"
            )
        ); */

        $message = $twilio->messages->create("whatsapp:+5214421371437", // to
                 array(
                     "from" => "whatsapp:+14155238886",
                     "body" => "Hola cuah como estas? Ya vi como se usa en sanbox :d"
                     )
        );

        print($message->sid);

    }
    public function index(){

        $notificaciones = Notificacion::orderBy('created_at', "DESC")->paginate(15);


        return View::make('Notificacion')
        ->with('notificaciones', $notificaciones);

    }
    public function store(Request $request){  
    try
    {

        $this->validate($request, [
            'titulo' => 'required',
            'icono' => 'required',
            'archivo' => 'required',
        ]);
    
        $notificacion = new Notificacion;

        $notificacion->titulo = $request->titulo;
        $notificacion->imagen = $request->imagen;
        $notificacion->contacto = $request->contacto;
        $notificacion->email = $request->email;
        $notificacion->telefono = $request->telefono;
        $notificacion->descripcion = $request->descripcion;
        $notificacion->link = $request->link;

        $file = $request->file('archivo');
        $archivo= Storage::putFile('notificaciones', $file, 'public');
        $notificacion->imagen = $archivo;
        
         
         $icono = $request->file('icono');
        $archivo2= Storage::putFile('icono', $icono, 'public');
        $notificacion->icono = $archivo2;
        
        $notificacion->save();

        Session::flash('mensaje', 'Notificacion enviada correctamente');
        
        OneSignal::sendNotificationToAll(
            $request->titulo, 
            $url = null, 
            $data = null, 
            $buttons = null, 
            $schedule = null
        );

        return redirect()->route('notificacion');
    }
       catch (FormValidationException $e)
        {
            // Failed.
            return redirect()->back()->withInput()->withErrors($e->getErrors());
        }

      
    }
    public function detalle($id){
        $compartidos = NotificacionCompartir::where('idNotificacion',$id)->orderBy('created_at')->get();
        $numeroCompartidos = NotificacionCompartir::where('idNotificacion',$id)->count();
        $no = NotificacionDetalle::where('idNotificacion',$id)->where('accion',0)->count();
        $si = NotificacionDetalle::where('idNotificacion',$id)->where('accion',1)->count();
        $vistas = NotificacionDetalle::where('idNotificacion',$id)->where('accion',3)->count('idAfiliado');
        
      /*  
        $noLista = NotificacionDetalle::where('idNotificacion',$id)->where('accion',0)->get();
        $siLista  = NotificacionDetalle::where('idNotificacion',$id)->where('accion',1)->get();
        $vistasLista  = NotificacionDetalle::where('idNotificacion',$id)->where('accion',3)->get();
*/
        
        
    $notificacion = Notificacion::find($id);
    $noLista = DB::select('SELECT * FROM `afiliados` where id in (SELECT idAfiliado from notificacionDetalle where idNotificacion = :id and accion = 0)',array('id' => $id));
    $siLista = DB::select('SELECT * FROM `afiliados` where id in (SELECT idAfiliado from notificacionDetalle where idNotificacion = :id and accion = 1)',array('id' => $id));
    $vistasLista = DB::select('SELECT * FROM `afiliados` where id in (SELECT idAfiliado from notificacionDetalle where idNotificacion = :id and accion = 3)',array('id' => $id));


        return View::make('detalleNotificacion')
        ->with(compact('compartidos'))
        ->with(compact('no'))
        ->with(compact('numeroCompartidos'))
        ->with(compact('si'))
        ->with(compact('vistas'))
        ->with(compact('noLista'))
        ->with(compact('siLista'))
        ->with(compact('vistasLista'))
        ->with(compact('notificacion'));
    }

    public function create(){

        return View::make('newNotificacion');
    }
}
