<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Notificacion;
use App\NotificacionCompartir;
use App\NotificacionDetalle;
use Mail;
use Auth;
class NotificacionApiController extends Controller
{

    public function login(Request $request){


        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);

        $rfc = $request->rfc;
        $password = $request->password;
    
    
        if (Auth::attempt(['rfc' => $rfc, 'password' => $password]))
        {
                $user = Auth::user();


               // return response()->json($user);
                return Response::json(array(
                  'code'      =>  201,
                  'user'   =>  $user
              ), 201);

        }
        else{
            return Response::json(array(
                  'code'      =>  401,
                  'message'   =>  "Contase単a incorecta"
              ), 401);
          }
    }
    public function lastLista(Request $request){
        $notificaciones = Notificacion::orderBy('created_at', 'desc')->take(10)->get();

        return Response::json(array(
            'code'      =>  201,
            'notificaciones'   =>  $notificaciones
        ), 201);

    }
    public function detalle($id,$idAfiliado){
        $notificacion = Notificacion::find($id);
        //Ver si ya ah hecho  una accion el usuario y mostrarlo
        $notificacionHecha = NotificacionDetalle::where("idNotificacion",$id)->where("idAfiliado",$idAfiliado)->first();

        return Response::json(array(
            'code'      =>  201,
            'notificacion'   =>  $notificacion,
            'notificacionHecha'   =>  $notificacionHecha
        ), 201);

    }
    public function sendInformation(Request $request){
        //sendInformation
        //Comprobar si el usuarioya ah hecho una accion en esta parte

        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);

    
        $notificacionHecha = NotificacionDetalle::where("idNotificacion",$request->idNotificacion)->where("idAfiliado",$request->idAfiliado)->where("accion",1)->orWhere("accion",0)->first();
        if($request->accion === 3){
            
            
               $notificacionHecha2 = NotificacionDetalle::where("idNotificacion",$request->idNotificacion)->where("idAfiliado",$request->idAfiliado)->where("accion",3)->first();
                   
            if($notificacionHecha2){
                    return Response::json(array(
                'code'      =>  201,
                'mensaje'   =>  "Guardado actualizado correctamente",
            ), 201);
            }else{
                  $notificacionDetalle = new NotificacionDetalle;
            $notificacionDetalle->idNotificacion = $request->idNotificacion;
            $notificacionDetalle->idAfiliado = $request->idAfiliado;
            $notificacionDetalle->accion = $request->accion;
            $notificacionDetalle->save();
            return Response::json(array(
                'code'      =>  201,
                'mensaje'   =>  "Guardado actualizado correctamente",
            ), 201);
            }
          
          
          
       
        }
        if( $notificacionHecha){

            $notificacionHecha->accion = $request->accion;
            $notificacionHecha->save();
            return Response::json(array(
                'code'      =>  201,
                'mensaje'   =>  "Actualizado correctamente",
            ), 201);
        }else{
            $notificacionDetalle = new NotificacionDetalle;
            $notificacionDetalle->idNotificacion = $request->idNotificacion;
            $notificacionDetalle->idAfiliado = $request->idAfiliado;
            $notificacionDetalle->accion = $request->accion;
            $notificacionDetalle->save();
            return Response::json(array(
                'code'      =>  201,
                'mensaje'   =>  "Guardado actualizado correctamente",
            ), 201);
        }
   
       
        //Actualizar 

        //Crear uno nuevo
    }
    public function compartir(Request $request){
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);

        
        try {
                $notificacion = Notificacion::find($request->idNotificacion);

                Mail::send('email.compartir',['notificacion' => $notificacion, 'nombre' => $request->nombre], function($message) use ($request) 
                {
                    $message->subject($request->tituloEvento);
                    $message->from('noreplay@visorindustrial.mx', 'AMIQRO');
                    $message->to($request->correo)->cc($request->correo);
                });
        
                
                $compartir = new NotificacionCompartir;
                $compartir->idAfiliado = $request->idAfiliado;
                $compartir->correo = $request->correo;
                $compartir->nombre = $request->nombre;
                $compartir->empresa = $request->empresa;

                
                $compartir->idNotificacion = $request->idNotificacion;
                $compartir->save();
        
                return Response::json(array(
                    'code'      =>  201,
                    'mensaje'   =>  'Mensaje Enviado'
                ), 201);
          } catch (Exception $e) {
            return Response::json(array(
                'code'      =>  401,
                'mensaje'   =>  'Error'
            ), 401);
          }

    }
    public function last(Request $request){
    }


}
