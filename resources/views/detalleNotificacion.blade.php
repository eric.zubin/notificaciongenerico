@extends('layouts.app')

@section('content')
<div class="row">
    <div class="container">
          <div class="d-flex justify-content-center">
                                      <h1>{{ $notificacion->titulo}}</h1>

             </div>
         <div class="d-flex justify-content-center">
           

             <div class="card" style="width: 100;">
                <img  src='{{URL::asset("/app/public/".$notificacion->imagen)}}' alt="Card image cap" height="100" width="100">
                <div class="card-body">
                </div>
            </div>
            
            <div class="card" style="width: 100;">
                <img class="card-img-top" src="{{URL::asset('/img/Check.png')}}" alt="Card image cap" height="100" width="100">
                <div class="card-body">
                    <p class="card-text">{{$vistas}}</p>
                </div>
            </div>
            <div class="card" style="width: 100;">
                <img class="card-img-top" src="{{URL::asset('/img/si.png')}}" alt="Card image cap" height="100" width="100">
                <div class="card-body">
                    <p class="card-text">{{$si}}</p>
                </div>
            </div>
              <div class="card" style="width: 100;">
                <img class="card-img-top" src="{{URL::asset('/img/no.png')}}" alt="Card image cap" height="100" width="100">
                <div class="card-body">
                    <p class="card-text">{{$no}}</p>
                </div>
            </div>
            <div class="card" style="width: 100;">
                <img class="card-img-top" src="{{URL::asset('/img/Share.png')}}" alt="Card image cap" height="100" width="100">
                <div class="card-body">
                    <p class="card-text">{{$numeroCompartidos}}</p>
                </div>
            </div>
            
              
            </div>
            
            
            
        </div>
            <div class="container">
                <div class="d-flex align-items-center">
                     <img src="{{URL::asset('/img/Share.png')}}" alt="Card image cap" height="100" width="100">

 
                </div>
              
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Asociado AMIQRO</th>
                                        <th scope="col">EMPRESA</th>

                    <th scope="col">Compartido A</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($compartidos as $compartido)
                    <tr>
                        <td> {{ $compartido->nombre }}</td>
                        <td> {{ $compartido->empresa }}</td>

                        
                        <td> {{ $compartido->correo }}</td>

                        
                    </tr>
                    @endforeach

                </tbody>
        </table>
                    </div>

                <div class="container">
                        <div class="d-flex align-items-center">
                <img  src="{{URL::asset('/img/no.png')}}" alt="Card image cap"  height="100" width="100">

 
                </div>
                  
            
                 <table class=" table">
            <thead>
                <tr>
                    <th scope="col">Asociado AMIQRO</th>
                                        <th scope="col">EMPRESA</th>

                </tr>
                </thead>
                <tbody>
                    @foreach ($noLista as $compartido )
                    <tr>
                                                            
                        <td> {{ $compartido->contacto }}</td>
                        <td> {{ $compartido->name_em }}</td>

                        

                        
                    </tr>
                    @endforeach

                </tbody>
        </table>
                </div>
        
                             <div class="container">
                            <div class="d-flex align-items-center">
                   <img src="{{URL::asset('/img/si.png')}}" alt="Card image cap" height="100" width="100">

 
                </div>    
                <table class="container table">
            <thead>
                <tr>
                    <th scope="col">Asociado AMIQRO</th>
                                        <th scope="col">EMPRESA</th>

                </tr>
                </thead>
                <tbody>
                    @foreach ($siLista as $compartido)
                    <tr>
                                              

                        <td> {{ $compartido->contacto }}</td>
                        <td> {{ $compartido->name_em }}</td>

                        

                        
                    </tr>
                    @endforeach

                </tbody>
        </table>
        
                </div>
              
                        
                                       <div class="container">

                  <div class="d-flex align-items-center">
                  <img  src="{{URL::asset('/img/Check.png')}}" alt="Card image cap" height="100" width="100">

 
                </div>

                <table class="container table">
            <thead>
                <tr>
                    <th scope="col">Asociado AMIQRO</th>
                                        <th scope="col">EMPRESA</th>

                </tr>
                </thead>
                <tbody>
                    @foreach ($vistasLista as $compartido)
                    <tr>
                                              

                        <td> {{ $compartido->contacto }}</td>
                        <td> {{ $compartido->name_em }}</td>

                        

                        
                    </tr>
                    @endforeach

                </tbody>
        </table>
        
                        </div>

                    </div>

            </div>


            
            
                        </div>

    </div>
</div>
<style>
.card{
    margin-right: 30px;
}
</style>
@endsection