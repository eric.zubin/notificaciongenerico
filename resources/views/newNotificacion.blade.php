@extends('layouts.app')

@section('content')
<style>
            .custom-file-control.selected:lang(en)::after {
                content: "" !important;
                }

                        #customFile .custom-file-control:lang(en)::after {
        content: "Select file...";
        }

        #customFile .custom-file-control:lang(en)::before {
        content: "Click me";
        }

            </style>     
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="container">

        <form  id="form1" method="POST" enctype="multipart/form-data" action="{{route('save_notificacion')}}" accept-charset="UTF-8">
            @csrf
            <div class="row">
                <div class="form-group col">
                    <label for="titulo">Titulo</label>
                    <input type="text" class="form-control" id="titulo" aria-describedby="titulo"  name="titulo" placeholder="Ingresa un titulo" required>
                </div>
            </div>
            
            <div class="row">
                <div class="form-group col">
                    <label for="contacto">Contacto</label>
                    <input type="text" class="form-control" id="contacto" aria-describedby="contacto"   name="contacto" placeholder="Ingresa un contacto" required>
                </div>
            </div>

            <div class="row">
                <div class="form-group col">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" aria-describedby="email"   name="email" placeholder="Ingresa un email" required>
                </div>
            </div>
            <div class="row">
                <div class="form-group col">
                    <label for="telefono">Telefono</label>
                    <input type="telefono" class="form-control" id="telefono"  name="telefono" aria-describedby="telefono" placeholder="Ingresa un telefono" required>
                </div>
            </div>

            <div class="row">
                <div class="form-group col">
                    <label for="link">Link</label>
                    <input type="link" class="form-control" id="link"  name="link" aria-describedby="link" placeholder="URL" required>
                </div>
            </div>

            <div class="custom-file">
                <input required type="file" class="custom-file-input" id="archivo" name="archivo"/>
                <label class="custom-file-label" for="archivo">Escoge archivo</label>
            </div>
            <br>


            <div class="custom-file">
                <input required type="file" class="custom-file-input" id="icono" name="icono"/>
                <label class="custom-file-label" for="icono">Escoge icono</label>
            </div>
            
            <div class="row">
                <div class="form-group col">
                    <label for="descripcion"> Descripcion</label>
                    <textarea class="form-control" id="descripcion" name="descripcion" rows="3" required></textarea>
                </div>
            </div>

        </form>
        <button type="submit"  class="btn btn-primary btn-lg btn-block" data-toggle="modal" data-target="#exampleModal">Guardar</button>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Comunicado</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        多Esta seguro de enviar el comunicado?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" id="guardar" class="btn btn-primary">Guardar</button>
      </div>
    </div>
  </div>
</div>
        <script>
        
        document.getElementById("guardar").addEventListener("click", function(){
                document.getElementById("form1").submit();

            });
            
            $('#exampleModal').on('shown.bs.modal', function () {
              $('#exampleModal').trigger('focus')
            })

            $('#archivo').on('change',function(){
                //get the file name
                var fileName = $(this).val();
                //replace the "Choose a file" label
                $(this).next('.custom-file-label').html(fileName);
            })
            
             $('#icono').on('change',function(){
                //get the file name
                var fileName = $(this).val();
                //replace the "Choose a file" label
                $(this).next('.custom-file-label').html(fileName);
            })
        </script>
      
</div>
@endsection