<?php

use Illuminate\Http\Request;
use App\Notificacion;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['cors']], function () {

    Route::post('/loginApp', 'NotificacionApiController@login')->name('api.login');
    //Hacer una ruta para devolver los ultmos 15 Ya hecho
    Route::get('/last_lista', 'NotificacionApiController@lastLista')->name('api.lastLista');
    //Hacer una ruta hacer los detalles Ya hecho
    Route::get('/detalle/{id}/{idAfiliado}', 'NotificacionApiController@detalle')->name('api.detalle');
    //Hacer una vista contestear que si
    Route::post('/sendInformation', 'NotificacionApiController@sendInformation')->name('api.sendInformation');
    //Hacer una vista para compartir Ya hecho
    Route::post('/compartir', 'NotificacionApiController@compartir')->name('api.compartir');
});



